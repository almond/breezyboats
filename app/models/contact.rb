class Contact < ActiveRecord::Base
  belongs_to :boat
  attr_accessible :email, :email_subject, :message, :name, :phone, :boat, :boat_id
end
