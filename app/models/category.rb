class Category < ActiveRecord::Base
	has_many :boats
  attr_accessible :description, :name, :slug
end
