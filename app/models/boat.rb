class Boat < ActiveRecord::Base
	has_many :photos
	has_many :contacts
	belongs_to :category

	SHIP_TYPES = [
		["Boats", "Boats"],
		["Manitou Pontoons", "MAnitou Pontoons"],
		["Pontoons", "Pontoons"],
		["Sail Boats", "Sail Boats"],
		["Trailers", "Trailers"],
	]

	has_attached_file :main_image, :styles => { :large => "554x360>", :medium => "300x230>", :small => "90x58>" }
	validates_attachment_size :main_image, :less_than => 100.megabytes


  attr_accessible :beam, :description, :engine,
  								:engine_hours, :hull_construction, :length,
  								:name, :price, :weight, :year, :type_string, :main_image, :category_id,
  								:brand, :model, :is_sold, :reduced_price

  def to_s
  	"#{id}, #{brand}, #{model}"
  end

end
