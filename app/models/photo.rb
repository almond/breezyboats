class Photo < ActiveRecord::Base
  attr_accessible :name, :pic, :boat, :boat_id
  belongs_to :boat
  has_attached_file :pic, :styles => { :large => "554x360>", :medium => "300x230>", :small => "90x58>" }
  validates_attachment_size :pic, :less_than => 100.megabytes
end
