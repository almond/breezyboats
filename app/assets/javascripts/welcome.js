jQuery(document).ready(function() {
	jQuery('#main-port').jcarousel(), jQuery('#our-clients').jcarousel();
});

$(document).ready(function(){
	$('#layerslider').layerSlider({
		skinsPath : '/assets/layerslider/skins/',
		skin : 'fullwidth',
		thumbnailNavigation : 'hover',
		hoverPrevNext : true,
		responsive : false,
		responsiveUnder : 940,
		sublayerContainer : 900
	});
});