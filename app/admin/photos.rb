ActiveAdmin.register Photo do
  form :html => { :enctype => "multipart/form-data" } do |f|
  	f.inputs do
  		f.input :boat
  		f.input :pic, :as => :file
  	end
  	f.buttons
  end
end
