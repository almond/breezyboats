ActiveAdmin.register Boat do
	index do
		column :id
		column :brand
		column :model
		column :length
		column :engine
		column :hull_construction
		column :price
		column :main_image do |boat|
			image_tag(boat.main_image.url(:small))
		end
		default_actions
	end

	form :html => { :enctype => "multipart/form-data" } do |f|
		f.inputs do
			f.input :brand
			f.input :model
			f.input :description
			f.input :length
			f.input :beam
			f.input :weight
			f.input :engine
			f.input :engine_hours
			f.input :hull_construction
			f.input :price
			f.input :category
			f.input :main_image, :as => :file
			f.input :is_sold
			f.input :reduced_price
		end
		f.buttons
	end
end
