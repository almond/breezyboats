work_dir = '/var/www/breezyboats/'


socket_path = "#{work_dir}tmp/sockets/unicorn.sock"
pid_path = "#{work_dir}tmp/pids/unicorn.pid"
err_log = "#{work_dir}logs/unicorn.stderr.log"
out_log = "#{work_dir}logs/unicorn.stdout.log"

worker_processes 2
working_directory "#{work_dir}/app"

preload_app true

timeout 30

listen socket_path, :backlog => 64

pid pid_path

stderr_path err_log
stdout_path out_log

before_fork do |server, worker|
  defined? (ActiveRecord::Base) and
    ActiveRecord::Base.connection.disconnect!
end

after_fork do |server, worker|
  defined? (ActiveRecord::Base) and
    ActiveRecord::Base.establish_connection
end


class Unicorn::HttpServer
  def proc_name(tag)
    $0 = ([ File.basename(START_CTX[0]), "breezyboats/tag",
            tag ]).concat(START_CTX[:argv]).join(' ')
  end
end
