class CreateBoats < ActiveRecord::Migration
  def change
    create_table :boats do |t|
      t.integer :year
      t.string :name
      t.text :description
      t.integer :length
      t.integer :beam
      t.integer :weight
      t.string :engine
      t.integer :engine_hours
      t.string :hull_construction
      t.float :price

      t.timestamps
    end
  end
end
