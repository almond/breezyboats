class ChangeColumnTypesForBoat < ActiveRecord::Migration
  def change
  	change_column :boats, :length, :string
  	change_column :boats, :beam, :string
  	change_column :boats, :weight, :string
  	change_column :boats, :engine, :string
  	change_column :boats, :engine_hours, :string
  	change_column :boats, :price, :string
  end
end
