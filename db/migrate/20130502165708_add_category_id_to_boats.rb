class AddCategoryIdToBoats < ActiveRecord::Migration
  def change
    add_column :boats, :category_id, :integer
  end
end
