class AddBoatIdToPhotos < ActiveRecord::Migration
  def change
    add_column :photos, :boat_id, :integer
  end
end
