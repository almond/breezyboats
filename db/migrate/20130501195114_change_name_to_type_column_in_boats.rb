class ChangeNameToTypeColumnInBoats < ActiveRecord::Migration
	def change
		rename_column :boats, :type, :type_string
	end
end
