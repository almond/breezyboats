class AddSoldAndReducedPriceFieldToBoats < ActiveRecord::Migration
  def change
    add_column :boats, :is_sold, :boolean
    add_column :boats, :reduced_price, :float
  end
end
