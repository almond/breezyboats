class RenameNameToBrand < ActiveRecord::Migration
  def change
  	rename_column :boats, :name, :brand
  end
end
