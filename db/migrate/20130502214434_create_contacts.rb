class CreateContacts < ActiveRecord::Migration
  def change
    create_table :contacts do |t|
      t.references :boat
      t.string :name
      t.string :email
      t.string :email_subject
      t.text :message
      t.string :phone

      t.timestamps
    end
    add_index :contacts, :boat_id
  end
end
