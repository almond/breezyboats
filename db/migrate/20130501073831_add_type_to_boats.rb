class AddTypeToBoats < ActiveRecord::Migration
  def change
    add_column :boats, :type, :string
  end
end
