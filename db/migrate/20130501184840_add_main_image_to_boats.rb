class AddMainImageToBoats < ActiveRecord::Migration
  def change
  	add_attachment :boats, :main_image
  end
end
