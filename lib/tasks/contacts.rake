namespace :contacts do
  desc "TODO"
  task :to_unicode => :environment do
		Contact.all.each do |contact|
			puts "Fixing #{contact.id}"
			if contact.message
				puts "Encoding is #{contact.message.encoding.name}"
				new_message = contact.message.encode("ISO-8859-1", :invalid => :replace)
				new_message = new_message.encode("UTF-8")
				puts "#{new_message}"
				contact.message = new_message
				contact.save
			end
		end
  end
end
